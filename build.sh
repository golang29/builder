#!/bin/bash

DEFAULT_PATH_BIN="bin"

COMMAND[0]="GOOS=android GOARCH=arm"
COMMAND[2]="GOOS=darwin GOARCH=amd64"
COMMAND[4]="GOOS=darwin GOARCH=arm64"
COMMAND[5]="GOOS=dragonfly GOARCH=amd64"
COMMAND[6]="GOOS=freebsd GOARCH=386"
COMMAND[7]="GOOS=freebsd GOARCH=amd64"
COMMAND[8]="GOOS=freebsd GOARCH=arm"
COMMAND[9]="GOOS=linux GOARCH=386"
COMMAND[10]="GOOS=linux GOARCH=amd64"
COMMAND[11]="GOOS=linux GOARCH=arm"
COMMAND[12]="GOOS=linux GOARCH=arm64"
COMMAND[13]="GOOS=linux GOARCH=ppc64"
COMMAND[14]="GOOS=linux GOARCH=ppc64le"
COMMAND[15]="GOOS=linux GOARCH=mips"
COMMAND[16]="GOOS=linux GOARCH=mipsle"
COMMAND[17]="GOOS=linux GOARCH=mips64"
COMMAND[18]="GOOS=linux GOARCH=mips64le"
COMMAND[19]="GOOS=netbsd GOARCH=386"
COMMAND[20]="GOOS=netbsd GOARCH=amd64"
COMMAND[21]="GOOS=netbsd GOARCH=arm"
COMMAND[22]="GOOS=openbsd GOARCH=386"
COMMAND[23]="GOOS=openbsd GOARCH=amd64"
COMMAND[24]="GOOS=openbsd GOARCH=arm"
COMMAND[25]="GOOS=plan9 GOARCH=386"
COMMAND[26]="GOOS=plan9 GOARCH=amd64"
COMMAND[27]="GOOS=solaris GOARCH=amd64"
COMMAND[28]="GOOS=windows GOARCH=386"
COMMAND[29]="GOOS=windows GOARCH=amd64"

read -p "Enter golang path: " path
read -p "Give a name to your package: " package_name
read -p "Enter version: " version

BIN_PATH=$path/$DEFAULT_PATH_BIN/$version

if [ ! -d "$path" ]; then
    echo -e "\n[!] Path doesn't exist..."
    error=true
fi

if [ -d "$BIN_PATH" ]; then
    echo -e "\n[!] Version already exist..."
    error=true
fi

if [  "$error" = true ]; then
    echo "Error detected, exit program"
    exit
fi

echo -e "\n[+] Folder is creating..."
mkdir -p $BIN_PATH

echo "[+] Compilation in progress..."
for command in "${COMMAND[@]}"; do
    OS=$(echo $command | cut -d " " -f 1 | cut -d "=" -f 2)
    PROCESSOR=$(echo $command | cut -d ' ' -f 2 | cut -d '=' -f 2)
    mkdir -p $BIN_PATH/$OS/$PROCESSOR
    export $command GOPATH="$(readlink -f $path)"; go build -o "$BIN_PATH/$OS/$PROCESSOR/$package_name" "$path"
done

echo -e "\n[+] Success ! \n"