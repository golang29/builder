# Go Builder

This script will build your golang code for all devices

## Requirement

You'll need gcc if you want compile for windows

## Usage

Clone this repository and make `build.sh` executable with:

```Shell
chmod u+x build.sh
```

when your golang project is ready to build, launch the script and answer to questions: 

```Shell
./build.sh
# Enter golang path = This path is the directory (absolute or relative) where are your *.go code
# Enter name to your package = The name you want give to your final package
# Enter version = The version you want give to your package
```

## Customization

You can edit the `build.sh` script if you need change the target path to all your binaries, by default it's the path you provide during the initialisation followed by `/bin` (the variable name is `DEFAULT_PATH_BIN`)

If you want go backward to the tree, feel free to use `../` or any relative path.

## Behaviour

This script will take care of the input field value to create directory and binary tree like:

`$PATH`/`bin`/`$VERSION`/`{OS}`/`{PROCESSOR}`/`$NAME`

## Advise

To automatize your binaries downloading, you can create a file into your git project who will print the last version so you'll be able to automatically curl this file in raw to get the last version like:

```Shell
curl -LO https://myrepo.com/$(curl -s https://gitlab.com/myproject/-/raw/master/version.txt)/linux/amd64/mybinary
```

it's a good practice to create this versionfile inside your code git project